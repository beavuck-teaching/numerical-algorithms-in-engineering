# TP Alg Num

To test TP_x, please navigate to the TP_x folder in shell or the command line terminal, then enter the following
command (you might have to install **GCC** first):

* to compile:

      gcc -Wall main.c -o main -lm

* to execute:

      ./main

[The related papers, in French](https://drive.google.com/drive/folders/1i3C5WG71kxQ5SO2DrW_7FRITi3ihOnID?usp=drive_link)

If you are testing TP_2 specifically, you can have a _*...poly.py_ file appear after execution. If you execute it
from shell or cmd, a scatterplot of the initial input will appear together with the graph of the interpolated polynomial
function. However, you might need to install **Python 3**, as well as the **numpy**, **sympy**, and **matplotlib** libraries

If you are testing TP_3 specifically, you can have a _.py_ file appear after execution. If you execute it
from shell or cmd, a scatterplot of the initial input will appear together with the graph of the regression curve.
However, you might need to install **Python 3**, as well as the **numpy**, and **matplotlib** libraries
